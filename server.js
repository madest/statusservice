const http = require('http');
const mysql = require('mysql');
const connection = mysql.createConnection({
  host     : '127.0.0.1',
  user     : 'adman',
  password : 'Envy85WHaps73bRO',
  database : 'adman_status'
});
const createSlackEventAdapter = require('@slack/events-api').createSlackEventAdapter;
const slackEvents = createSlackEventAdapter("Cg3IoOj2GlDbNoL7meyG6r3W");
const port = 35861;

connection.connect(function(err) {
  if (err) {
    console.error('error connecting: ' + err.stack + 'connected as id ' + connection.threadId);
    return err;
  }
});

function creatTableUsers() {
 connection.query("CREATE TABLE IF NOT EXISTS users (id INT(25) NOT NULL AUTO_INCREMENT, name varchar(100) DEFAULT NULL, status varchar(100) DEFAULT NULL, time_change varchar(100) DEFAULT NULL, PRIMARY KEY (id))");
};
function insertIntoUsers(name, status, update, image) {
 connection.query("INSERT INTO users(name, status, time_change, image) VALUES ('" + name + "', '" + status + "', '" + update + "', '" + image + "')");
};
function updateIntoUsers(id, status, update){
 connection.query("UPDATE users SET status = '" + status + "', time_change = '"+ update +"' WHERE id = "+ id);
};
function getUsers(){
 connection.query("SELECT * FROM users", function(err, response){
 	return response;
 });
};
function onRequest(request, response){
	if(request.url === "/api"){
		let promise = new Promise((resolve, reject) => {
			connection.query("SELECT * FROM users", function(err, res){			 	
				if (err) reject(err);		
				if (res) resolve(res);
			});
		});
		promise.then((res) => {
			response.writeHead(200, {'Content-Type': 'application/json'});
			response.write(JSON.stringify(res));
			response.end();
		});				
	}else{
		response.end();
	}	
};


let handler = function(data){
  let name = data.user.name;
  let status = data.user.profile.status_text;
  let update = data.user.updated;
  let image = data.user.profile.image_512;

  let promise = new Promise((resolve, reject) => {
	connection.query("SELECT * FROM users WHERE name = '" + name + "'",
	function (err, response) {
		if (err) reject(err);		
		if (response) resolve(response);
 	});
  });

  promise.then((response) => {
  	  let old;
  	  let date = new Date();
  	  let month = date.getMonth() + 1;
  	  let format = date.getDate() + '.' + month + '.' + date.getFullYear() + ' ' + date.getHours() + ':' + date.getMinutes();
 	  for(let item of response)	old = item;

      if(JSON.stringify(response) === "[]"){
	  	insertIntoUsers(name, status, format, image);
	  }else{	    
	  	if(old.status !== status) updateIntoUsers(old.id, status, format);
	  }
  });
};

http.createServer(onRequest).listen(35862);
slackEvents.on('user_change', (data) => {handler(data);});
slackEvents.on('error', console.error);
slackEvents.start(port).then(() => {console.log(`server listening on port ${port}`);});