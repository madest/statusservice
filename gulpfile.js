"use strict";

var gulp = require('gulp'),
	rename = require('gulp-rename'),
    less = require('gulp-less'),
    cssmin = require('gulp-cssmin'),
    LessPluginAutoPrefix = require('less-plugin-autoprefix'),
    autoprefix = new LessPluginAutoPrefix({browsers: ["last 2 versions"]});

gulp.task('less', function() {
	return gulp
		.src("webapp/src/style.less")
		.pipe(less({plugins: [autoprefix]}))
		.pipe(cssmin())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest("webapp/dist/"));
});